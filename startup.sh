#!/bin/bash

HOSTS_FILE_TMP=/tmp/hosts

MASTER_HOST=${MASTER_HOST:-master}
MASTER_IP=${MASTER_IP:-0.0.0.0}
NODE_TYPE=${NODE_TYPE:-node}
SLAVES=${SLAVES:-localhost}
NODE_MANAGER_WEB_PORT=${NODE_MANAGER_WEB_PORT:-8042}

sed -i -e "s|@MASTER_IP@|${MASTER_HOST}|g" ${HADOOP_CONF_DIR}/yarn-site.xml
sed -i -e "s|@NODE_MANAGER_WEB_PORT@|${NODE_MANAGER_WEB_PORT}|g" ${HADOOP_CONF_DIR}/yarn-site.xml
sed -i -e "s|@MASTER_IP@|${MASTER_HOST}|g" ${HADOOP_CONF_DIR}/core-site.xml

#CLUSTER_NAME=${CLUSTER_NAME:-elasticsearch}
#NODE_MASTER=${NODE_MASTER:-true}
#NODE_DATA=${NODE_DATA:-true}
#INDEX_NUMBER_OF_SHARDS=${INDEX_NUMBER_OF_SHARDS:-5}
#INDEX_NUMBER_OF_REPLICAS=${INDEX_NUMBER_OF_REPLICAS:-1}
#BOOTSTRAP_MLOCKALL=${BOOTSTRAP_MLOCKALL:-false}
#TRANSPORT_TCP_PORT=${TRANSPORT_TCP_PORT:-9300}
#HTTP_PORT=${HTTP_PORT:-9200}

#CONFIG_FILE='/opt/elasticsearch/config/elasticsearch.yml'

#sed -i -e "s|@CLUSTER_NAME|${CLUSTER_NAME}|g" $CONFIG_FILE
#sed -i -e "s|@NODE_MASTER|${NODE_MASTER}|g" $CONFIG_FILE
#sed -i -e "s|@NODE_DATA|${NODE_DATA}|g" $CONFIG_FILE
#sed -i -e "s|@INDEX_NUMBER_OF_SHARDS|${INDEX_NUMBER_OF_SHARDS}|g" $CONFIG_FILE
#sed -i -e "s|@INDEX_NUMBER_OF_REPLICAS|${INDEX_NUMBER_OF_REPLICAS}|g" $CONFIG_FILE
#sed -i -e "s|@BOOTSTRAP_MLOCKALL|${BOOTSTRAP_MLOCKALL}|g" $CONFIG_FILE
#sed -i -e "s|@TRANSPORT_TCP_PORT|${TRANSPORT_TCP_PORT}|g" $CONFIG_FILE
#sed -i -e "s|@HTTP_PORT|${HTTP_PORT}|g" $CONFIG_FILE

#export ES_HEAP_SIZE=${ES_HEAP_SIZE}

if [ -f "$HOSTS_FILE_TMP" ]; then
    cat $HOSTS_FILE_TMP >> /etc/hosts
fi


#HADOOP_PREFIX=/usr/local/hadoop

echo "${MASTER_IP} ${MASTER_HOST}" >> /etc/hosts

# Start Service
service ssh start

if [ ${NODE_TYPE} == "master" ]; then
    echo "" > ${HADOOP_CONF_DIR}/slaves
    hosts=(${SLAVES//,/ })
    for i in "${hosts[@]}"
        do
            : 
            echo $i >> ${HADOOP_CONF_DIR}/slaves
    done
    echo "Formating namenode..."
    $HADOOP_PREFIX/bin/hdfs namenode -format
    echo "Starting HDFS..."
    $HADOOP_PREFIX/sbin/start-dfs.sh
    echo "Starting YARN..."
    $HADOOP_PREFIX/sbin/start-yarn.sh
elif [ ${NODE_TYPE} == "slave" ]; then
    $HADOOP_PREFIX/sbin/hadoop-daemon.sh start datanode
    $HADOOP_PREFIX/sbin/yarn-daemon.sh start nodemanager
fi

supervisord -n
# Keep container running


